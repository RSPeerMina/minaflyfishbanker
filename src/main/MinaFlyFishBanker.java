package main;

import gui.StartGui;
import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Production;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.ItemTableListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.ItemTableEvent;
import org.rspeer.runetek.event.types.ItemTableEvent.ChangeType;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;
import java.time.Instant;
import java.util.function.Predicate;


@ScriptMeta(
        name = "MinaFlyFishBanker ",
        desc = "FlyFish at barbarian village 5 modes",
        developer = "Mina",
        category = ScriptCategory.FISHING
)
public class MinaFlyFishBanker extends Script implements RenderListener, ItemTableListener {

    private static final Position DES = new Position(3105, 3431);
    private static StartGui startGui;
    private int troutsCaught, salmonsCaught, troutsCooked, salmonsCooked, fishingXP, cookingXP;
    private StopWatch timer;
    private Predicate<Item> TO_DROP = item -> item.getName().contains("Raw");
    private Predicate<Item> TO_DROP_BURNT = item -> item.getName().contains("Burnt");
    private Predicate<Item> TO_DROP_COOKED = item -> item.containsAction("Eat");
    private Instant lastAnimation;

    private boolean isAnimating() {
        return Players.getLocal().isAnimating();
    }

    private boolean isMoving() {
        return Players.getLocal().isMoving();
    }

    public void onStart() {
        if (!Inventory.contains("Feather") || !Inventory.contains("Fly fishing rod")) {
            Bank.open();
            Time.sleepUntil(Bank::isOpen, 2000, 3500);
        }

        if (Bank.isOpen()) {
            Bank.depositAllExcept("Fly fishing rod", "Feather");
            if (!Inventory.contains("Fly fishing rod")) {
                if (Bank.contains(z -> z.getName().contains("Fly"))) {
                    Bank.withdraw("Fly fishing rod", 1);
                } else {
                    Log.severe("We have no rod");
                    setStopping(true);
                }
            }
            if (!Inventory.contains("Feather") && Inventory.contains("Fly fishing rod")) {
                if (Bank.contains(x -> x.getName().equals("Feather"))) {
                    Bank.withdrawAll("Feather");
                } else {
                    Log.severe("We have no feathers");
                    setStopping(true);
                }
            }
        }
        startGui = new StartGui();
        timer = StopWatch.start();
    }


    private void walkToSpot() {
        Movement.walkToRandomized(DES);
        Time.sleepUntil(() -> DES.distance() <= 15, 2000, 3500);
    }

    private void cookTrout() {
        SceneObject FIRE = SceneObjects.getNearest("Fire");

        if (FIRE != null) {
            if (lastAnimation == null || lastAnimation.plusMillis(2000).isBefore(Instant.now())) {
                if (!Production.isOpen()) {
                    Inventory.getFirst("Raw trout").interact("Use");
                    FIRE.interact("Use");
                } else {
                    Production.initiate();
                    Time.sleepUntil(() -> isAnimating(), 1000, 2000);
                }
            }
        }
    }

    private void cookSalmon() {
        SceneObject FIRE = SceneObjects.getNearest("Fire");

        if (FIRE != null) {
            if (lastAnimation == null || lastAnimation.plusMillis(2000).isBefore(Instant.now())) {
                if (!Production.isOpen()) {
                    Inventory.getFirst("Raw salmon").interact("Use");
                    FIRE.interact("Use");
                } else {
                    Production.initiate();
                    Time.sleepUntil(() -> isAnimating(), 1000, 2000);
                }
            }
        }
    }

    private void bankFish() {
        if (!Bank.isOpen()) {
            Bank.open();
            Time.sleepUntil(Bank::isOpen, 2000, 3500);
        } else {
            if (Bank.contains("Feather")) {
                Bank.withdrawAll("Feather");
                Time.sleepUntil(() -> !Bank.contains("Feather"), 2000, 3000);
            }

            Bank.depositAllExcept(314, 309);

            if (!Inventory.contains("Feather") && !Bank.contains("Feather")) {
                Log.info("We're out of feathers, time to stop");
                setStopping(true);
            }
        }
    }

    public int loop() {
        if (Movement.getRunEnergy() > Random.low(15, 35) && !Movement.isRunEnabled()) {
            Movement.toggleRun(true);
            return 1000;
        }

        if (startGui.selectedMode() != 5) {
            switch (startGui.selectedMode()) {
                case 0:
                    this.setTitlePaneMessage("Bank" + " | Runtime: " + timer.toElapsedString());
                    break;
                case 1:
                    this.setTitlePaneMessage("Drop" + " | Runtime: " + timer.toElapsedString());
                    break;
                case 2:
                    this.setTitlePaneMessage("F1D1" + " | Runtime: " + timer.toElapsedString());
                    break;
                case 3:
                    this.setTitlePaneMessage("Cook and Drop" + " | Runtime: " + timer.toElapsedString());
                    break;
                case 4:
                    this.setTitlePaneMessage("Cook and Bank" + " | Runtime: " + timer.toElapsedString());
                    break;
                default:
                    this.setTitlePaneMessage("Choosing mode" + " | Runtime: " + timer.toElapsedString());
                    break;
            }
        }

        if (startGui.isStarted()) {
            fishingXP = (getSalmonsCaught() * 70 + (getTroutsCaught() * 50));
            cookingXP = (getSalmonsCooked() * 90 + (getTroutsCooked() * 70));

            if (Players.getLocal().getAnimation() == 897) {
                lastAnimation = Instant.now();
            }

            if (Inventory.isFull()) {
                switch (startGui.selectedMode()) {
                    case 0:
                        bankFish();
                        break;
                    case 1:
                        powerDrop(TO_DROP, Random.high(80, 175), Random.high(175, 220));
                        break;
                    case 3:
                        if (DES.distance() <= 10) {
                            if (!isAnimating() && !isMoving()) {
                                if (Skills.getLevel(Skill.COOKING) >= 25) {
                                    if (Inventory.contains("Raw salmon")) {
                                        cookSalmon();
                                    } else {
                                        if (Inventory.contains("Raw trout")) {
                                            cookTrout();
                                        } else {
                                            powerDrop(TO_DROP_BURNT, Random.high(80, 175), Random.high(175, 220));
                                            powerDrop(TO_DROP_COOKED, Random.high(80, 175), Random.high(175, 220));
                                        }
                                    }
                                } else if (Skills.getLevel(Skill.COOKING) >= 15) {
                                    if (Inventory.contains("Raw trout")) {
                                        cookTrout();
                                    } else {
                                        powerDrop(TO_DROP, Random.high(80, 175), Random.high(175, 220));
                                        powerDrop(TO_DROP_BURNT, Random.high(80, 175), Random.high(175, 220));
                                        powerDrop(TO_DROP_COOKED, Random.high(80, 175), Random.high(175, 220));
                                    }
                                } else {
                                    Log.severe("Too low cooking, dropping");
                                    powerDrop(TO_DROP, Random.high(80, 175), Random.high(175, 220));
                                }
                            }
                        } else {
                            walkToSpot();
                        }
                        break;
                    case 4:
                        if (!isAnimating() && !isMoving()) {
                            if (Skills.getLevel(Skill.COOKING) >= 25) {
                                if (Inventory.contains("Raw salmon")) {
                                    cookSalmon();
                                } else {
                                    if (Inventory.contains("Raw trout")) {
                                        cookTrout();
                                    } else {
                                        if (Inventory.contains(TO_DROP_BURNT)) {
                                            powerDrop(TO_DROP_BURNT, Random.high(80, 175), Random.high(175, 220));
                                        } else {
                                            bankFish();
                                        }
                                    }
                                }
                            } else if (Skills.getLevel(Skill.COOKING) >= 15) {
                                if (Inventory.contains("Raw trout")) {
                                    cookTrout();
                                } else {
                                    if (Inventory.contains(TO_DROP_BURNT)) {
                                        powerDrop(TO_DROP_BURNT, Random.high(80, 175), Random.high(175, 220));
                                    } else {
                                        bankFish();
                                    }
                                }
                            } else {
                                Log.severe("Too low cooking, dropping");
                                powerDrop(TO_DROP, Random.high(80, 175), Random.high(175, 220));
                            }
                        }
                        break;
                }
            }

            if (!Inventory.isFull() && startGui.selectedMode() != 5) {
                if (DES.distance() <= 15) {
                    Npc FISHSPOT = Npcs.getNearest(x -> x.getName().equals("Rod Fishing spot") && x.containsAction("Lure"));

                    if (!Inventory.contains("Feather") && !Bank.contains("Feather")) {
                        Log.info("We're out of feathers, time to stop");
                        setStopping(true);
                    }

                    if (startGui.selectedMode() != 2) {
                        if (!isAnimating() && !isMoving()) {
                            if (FISHSPOT != null) {
                                FISHSPOT.interact("Lure");
                                Time.sleepUntil(() -> isAnimating(), 1000, 2000);
                            } else {
                                walkToSpot();
                            }
                        }
                    } else {
                        Interactable raw = Inventory.getFirst(item -> item.getName().contains("Raw"));
                        if (raw != null) {
                            raw.interact("Drop");
                            FISHSPOT.interact("Lure");
                        } else {
                            if (!isAnimating()) {
                                FISHSPOT.interact("Lure");
                                Time.sleepUntil(() -> isAnimating(), 1000, 2000);
                            }
                        }
                    }
                } else {
                    walkToSpot();
                }
            }
        }

        return 1000;
    }

    private void powerDrop(Predicate<Item> condition, int minSleep, int maxSleep) {
        int order[] = {
                0, 4, 8, 12, 16, 20, 24, 1, 5, 9, 13, 17, 21, 25, 2, 6, 10, 14, 18, 22, 26, 3, 7, 11, 15, 19, 23, 27
        };
        for (int i : order) {
            Item item = Inventory.getItemAt(i);
            if (item != null && condition.test(item)) {
                item.interact("Drop");
                Time.sleep(minSleep, maxSleep);
            }
        }
    }

    private int getTroutsCaught() {
        return troutsCaught;
    }

    private int getSalmonsCaught() {
        return salmonsCaught;
    }

    private int getTroutsCooked() {
        return troutsCooked;
    }

    private int getSalmonsCooked() {
        return salmonsCooked;
    }

    public void notify(ItemTableEvent e) {
        if (DES.distance() <= 20) {
            if (e.getChangeType() == ChangeType.ITEM_ADDED && e.getId() == 335) {
                troutsCaught++;
            }

            if (e.getChangeType() == ChangeType.ITEM_ADDED && e.getId() == 331) {
                salmonsCaught++;
            }

            if (e.getChangeType() == ChangeType.ITEM_ADDED && e.getId() == 333) {
                troutsCooked++;
            }

            if (e.getChangeType() == ChangeType.ITEM_ADDED && e.getId() == 329) {
                salmonsCooked++;
            }
        }
    }

    public void notify(RenderEvent e) {
        Graphics g = e.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int y = 35;
        int x = 10;

        g2.setColor(Color.MAGENTA);
        g2.drawString("Trouts caught: " + getTroutsCaught() + " (" + (int) timer.getHourlyRate(getTroutsCaught()) + " /H)", x, y);
        g2.drawString("Salmons caught: " + getSalmonsCaught() + " (" + (int) timer.getHourlyRate(getSalmonsCaught()) + " /H)", x, y += 20);
        switch (startGui.selectedMode()) {
            case 3:
            case 4:
                g2.drawString("Trouts cooked: " + getTroutsCooked() + " (" + (int) timer.getHourlyRate(getTroutsCooked()) + " /H)", x, y += 20);
                g2.drawString("Salmons cooked: " + getSalmonsCooked() + " (" + (int) timer.getHourlyRate(getSalmonsCooked()) + " /H)", x, y += 20);
                break;
        }

        g2.drawString("Fishing XP: " + fishingXP + " (" + (int) timer.getHourlyRate(fishingXP) / 1000 + " k/H)", x, y += 20);
        switch (startGui.selectedMode()) {
            case 3:
            case 4:
                g2.drawString("Cooking XP: " + cookingXP + " (" + (int) timer.getHourlyRate(cookingXP) / 1000 + " k/H)", x, y += 20);
                break;
        }

    }

    public void onStop() {
        Log.fine("Thank you for using MinaFlyFishBanker, we caught " + (salmonsCaught + troutsCaught) + " fishes!");
        super.onStop();
    }
}