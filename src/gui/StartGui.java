package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartGui extends Gui {
    public static int selectedMode;
    private boolean started = false;

    public StartGui() {
        super();
        mainPanel.setBackground(Color.BLACK);
        mainPanel.setForeground(Color.WHITE);
        mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(mainPanel);
        mainPanel.setLayout(null);

        Vendor[] mode = {new Vendor("Bank", 0)
                , new Vendor("Drop", 1)
                , new Vendor("F1D1", 2)
                , new Vendor("CookNDrop", 3)
                , new Vendor("CookNBank", 4)};

        JLabel lblSelectMode = new JLabel("Select mode");
        lblSelectMode.setForeground(Color.CYAN);
        lblSelectMode.setBounds(10, 11, 84, 14);

        mainPanel.add(lblSelectMode);

        JComboBox<Object> cBMode = new JComboBox<Object>(mode);
        cBMode.setBounds(93, 8, 124, 20);
        cBMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Vendor item = (Vendor) cBMode.getSelectedItem();
                selectedMode = item.ID;
            }
        });

        mainPanel.add(cBMode);

        JButton btnStartBot = new JButton("Start");
        btnStartBot.setForeground(Color.CYAN);
        btnStartBot.setBounds(75, 38, 89, 23);
        btnStartBot.addActionListener(e -> {

            started = true;
            setVisible(false);

        });
        mainPanel.add(btnStartBot);
        setVisible(true);

    }

    public boolean isStarted() {
        return started;
    }

    public int selectedMode() {
        return selectedMode;
    }

}
