package gui;

public class Vendor {
    String name;
    int ID;

    public Vendor(String name, int ID) {
        this.name = name;
        this.ID = ID;
    }

    @Override
    public String toString() { return name; }
}
